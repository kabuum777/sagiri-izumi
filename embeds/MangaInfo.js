module.exports = (data, message) => {

        const base_url = "https://nhentai.net";
        const base_gallery = "https://t.nhentai.net/galleries";

        const TYPE = {
            j: "jpg",
            p: "png",
            g: "gif"
        };

        let tags = '';

        data.tags.forEach(tag => {
            if((tags.length + `[${tag.name}](${base_url}${tag.url}) `.length) <= 1024){
                tags += `[${tag.name}](${base_url}${tag.url}) `;
            }
        });

        const embed = {
            "color": 15541587,
            "title":`${data.title.pretty}`,
            "url":`${base_url}/g/${data.id}`,
            "thumbnail": {
                "url": "http://i.imgur.com/uLAimaY.png"
            },
            "image": {
                "url": `${base_gallery}/${data.media_id}/cover.${TYPE[data.images.cover.t]}`
            },
            "author": {
                "name": `${data.id}`,
                "url": "",
                "icon_url": ""
              },
            "fields":[
            {
                "name":"Pages",
                "value":`${data.num_pages}`,
                "inline":"false"
            },
            {
                "name":"Tags",
                "value":`${tags}`,
                "inline":"false"
            }]};
      message.channel.send({ embed });  
    };