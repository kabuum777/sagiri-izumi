module.exports  = (client, message) => {
    //Ignore bots
    if(message.author.bot) return;
    //Chceck if message start with prefix
    if(message.content.indexOf(client.config.prefix) !== 0) return;

    const args = message.content.slice(client.config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();


    const cmd = client.commands.get(command);

    if(!cmd) return 0;

    let timestamp = new Date().toUTCString();
    console.log(`${timestamp} : ${command}[${args}]`);

    cmd.run(client, message, args);
}