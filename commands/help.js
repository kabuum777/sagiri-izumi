exports.run = (client, message, args) => {
    const embed = {
        "color": 15541587,
        "title":`Help`,
        "url": "",
        "image": {
            "url": ""
        },
        "author": {
            "name": `Sagir Izumi v0.3`,
            "url": "",
            "icon_url": ""
            },
        "fields":[
        {
            "name":"Help",
            "value":"Show this message.",
            "inline":"false"
        },
        {
            "name": "Info",
            "value": "Show info about doujinshi.",
            "inline": "false"
        },
        {
            "name": "random",
            "value": "Show random doujinshi",
            "inline": "false"
        }
    ]};

    message.channel.send({ embed });
};